import argparse
import time
import paho.mqtt.client as mqtt

__protocol_versions__ = {"v31": mqtt.MQTTv31, "v311": mqtt.MQTTv311, "v5": mqtt.MQTTv5}

class Publisher(object):
    def __init__(self, **kwargs):
        self.period = 1.0/kwargs.get('freq', 60)
        self.host = kwargs.get('host', "localhost")
        self.port = kwargs.get('port', 1883)
        self.time = kwargs.get('time', 0)
        self.topic = kwargs.get('topic',"")
        self.qos = kwargs.get('qos', 0)
        self.name = kwargs.get('name')
        self.version = kwargs.get('version')
        self.client = mqtt.Client(client_id=self.name, protocol=self.version)
        self.client.on_connect = self._on_connect
        self.name = self.client._client_id.decode('utf-8')
        self.msg_count = 0
        self.last_loop = 0
        self.time_ctl = 0
    
    def _on_connect(self, client, userdata, flags, rc):
        if rc==0:
            print("Connected to {}:{}".format(self.host,self.port))
        else:
            raise ConnectionError("Could not connect to {}:{}".format(self.host,self.port))

    def connect(self):
        self.client.connect(self.host, self.port)

    def disconnect(self):
        self.client.disconnect()

    def loop(self):
        if self.time:
            if self.time_ctl == 0:
                self.time_ctl = time.time()
            if (time.time() - self.time_ctl) >= self.time:
                return False
        
        if time.time() - self.last_loop >= self.period:
            self.last_loop = time.time()
            self.client.publish(
                topic=self.topic,
                payload="{0},{1},{2:.6f}".format(self.name ,self.msg_count, time.time()),
                qos=self.qos
            )
            self.msg_count += 1
            self.client.loop()
        return True
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--freq", help="Frequency of publish messages (def. 60)", type=float, default=60)
    parser.add_argument("-ho", "--host", help="MQTT broker address e.g. 192.168.0.1", type=str, default="localhost")
    parser.add_argument("-p", "--port", help="MQTT broker port (def. 1883)", type=int, default=1883)
    parser.add_argument("-to", "--topic", help="MQTT topic (def. benchmark", type=str, default="benchmark")
    parser.add_argument("-n", "--name", help="Client name", type=str)
    parser.add_argument("-q", "--qos", help="MQTT Quality of Service (def. 0)", choices=[0,1,2], type=int, default=0)
    parser.add_argument("-t", "--time", help="Publish messages for t seconds", type=int, default=0)
    parser.add_argument("-v", "--version", help="MQTT protocol version (def. v31", choices=['v31', 'v311', 'v5'], type=str, default='v31')
    args = parser.parse_args()
    args.version = __protocol_versions__[args.version]
    
    publisher = Publisher(**args.__dict__)
    publisher.connect()
    while publisher.loop():
        pass
    publisher.disconnect()
