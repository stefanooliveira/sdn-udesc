import argparse
import numpy as np
import matplotlib.pyplot as plt
import statistics as stc

# Para executar: python3 benchmark.py -f t1_noia_stef.csv t1_noia_vo.csv -w 15 -p

class Benchmark:

    def __init__(
                 self, 
                 **kwargs
                 ):
        
        self._filenames = kwargs.get('files')
        if len(self._filenames) == 0:
            raise Exception("No file has been passed.")

        self._plot_packages_lost = kwargs.get('plot_package_lost', True)

        self.window_size = kwargs.get('window_filter_size', 15)

        self._array_of_files = self.read_files(self._filenames)

    def read_files(self, filenames):

        files_matrices = []

        for file in filenames:
            try:
                f = np.genfromtxt(file,delimiter=',')
                files_matrices.append(f)
            except Exception as e:
                print ("Error opening the files: ")
                print (e)
        
        return np.array(files_matrices, dtype=object)

    def _slide_window_filter(self, vector):
        new_vector = []
        i = 0
        vector_size = len(vector)

        while (i < vector_size ):

            sample_of_the_time = []
            j = 0
            while (j < self.window_size ):

                if (i + j) >= vector_size:
                    index = (i + j) - vector_size
                    sample_of_the_time.append( vector[index] )
                else:
                    sample_of_the_time.append( vector[i + j] )

                j += 1

            new_vector.append( [stc.mean(sample_of_the_time), stc.stdev(sample_of_the_time)] )
            i += 1

        return np.array(new_vector)

    def _define_a_positionFilter(self, original_size):
        _filter = []
        i = 0
        while (i < original_size):
            if ((i%self.window_size) == 0) and (i != 0):
                k = i - int(self.window_size/2)
                _filter[k] = True

            _filter.append(False)
            i += 1

        return np.array(_filter)

    def _write_frequency_at_window(self, axs, filter, data, data_filtered):
        
        font = {'size' : 6.4}

        all_index = np.arange(0,len(data),1)

        indexes = all_index[filter]

        up = 0.04
        right = 0

        for i in indexes:

            final_windowIndex = i + int(self.window_size/2)

            if final_windowIndex >= len(data):
                final_windowIndex = len(data) - 1

            time_final_window = data[final_windowIndex,0]
            initial_windowIndex = i - int(self.window_size/2)
            time_initial_window = data[initial_windowIndex,0]

            total_time = time_final_window - time_initial_window
            N_samples = final_windowIndex - initial_windowIndex + 1 # exemplo, 5 - 3 = 2. Porem foram-se 3 samples, logo, deve-se por o + 1.

            frequency = int (N_samples/float(total_time))

            axs.text( data[i,0] + right, data_filtered[i,0] + up, str(frequency), style='oblique', fontdict=font )

 
    def _func_plot_delay(self):

        fig, axs = plt.subplots(nrows=1, ncols=1)
        plt.grid(True,alpha=0.3,which='both')

        for file, f_name in zip(self._array_of_files, self._filenames):
            delay_filtered = self._slide_window_filter(file[:,1])

            frequency = round(len(file)/float((file[-1,0] - file[0,0])), 3)

            plot_instance = axs.plot(file[:,0], delay_filtered[:,0], label=f_name + f" - Em {frequency} Hz",linewidth=2)

            _Err_filter = self._define_a_positionFilter(len(file))
            axs.errorbar(
                         file[_Err_filter][:,0], 
                         delay_filtered[_Err_filter][:,0], 
                         yerr = delay_filtered[_Err_filter][:,1]/2.0, 
                         fmt='o', 
                         capsize=4,
                         color=plot_instance[-1].get_color(),
                         linewidth=0.5
                         )

            # self._write_frequency_at_window(axs,_Err_filter, file, delay_filtered)

        axs.legend(
                  ncol=1, 
                  fancybox=True, 
                  shadow=True,
                  loc="upper right",
                  bbox_to_anchor=(1.02,1.15)
                  )
        axs.set_xlabel("Tempo (s)")
        axs.set_ylabel("Tempo de resposta (ms)")
        return fig, axs

    def _count_packages_lost(self, n_seqs):
        lost = 0
        previus = 0
        for nseq in n_seqs:
            if (nseq - previus) > 1:
                lost += (nseq - previus)
            previus = nseq
        return [len(n_seqs) - lost, lost]

    def _plot_pie(self):
        for file, f_name in zip(self._array_of_files, self._filenames):   
            
            labels = ['Recebido','Perdido']
            stats = self._count_packages_lost(file[:,2])

            fig, axs = plt.subplots(nrows=1, ncols=1)

            axs.pie(
                    stats,
                    labels=labels, 
                    autopct='%1.1f%%',
                    shadow=True, 
                    startangle=90
                    )

            
            axs.legend(labels,
                      loc="center left",
                      bbox_to_anchor=(1, 0, 0.5, 1)
                      )

            axs.set_title(f_name)

            plt.show()


    def plot(self):
        fig, axs = self._func_plot_delay()
        fig.set_size_inches(5.85,4.2)
        plt.show()

        if (self._plot_packages_lost == True):
            self._plot_pie()



if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-f",
                        "--files", 
                        action='store',
                        type=str, 
                        help="<Required> Files to be analysed. Ex: -f fileA.csv fileB.csv ...",
                        nargs='*',
                        required=True)

    parser.add_argument("-w",
                        "--window_filter_size",
                        type=int, 
                        help="Size of the window that is used in the slided-window filter (def: 15)."
                        )

    parser.add_argument("-p",
                        "--plot_package_lost",
                        default=False,
                        action='store_true',
                        help="Type -p if you wishe to plot missing packeges graphic (def: False)."
                        )

    args = parser.parse_args()

    benchmark = Benchmark(**args.__dict__)

    benchmark.plot()
