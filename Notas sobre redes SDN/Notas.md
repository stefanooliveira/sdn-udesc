# Redes Definidas por Software (SDN)

> Quando eu era pesquisadora na AT&T, um grupo nosso projetou uma nova forma de gerenciar o roteamento nas redes de *backbone* dos ISPs. Em geral, os operadores de rede configuram cada roteador individualmente, e esses roteadores executam protocolos distribuídos para calcular caminhos através da rede [...] pesquisadores e profissionais estão começando a projetar redes que são fundamentalmente mais fáceis de gerenciar. Como nosso trabalho inicial no RCP, a ideai principal nas chamadas **redes definidas por software (SDN - *Software Defined Networking*)** é executar um controlador que possa instalar regras de tratamento de pacotes de baixo nível nos computadores subjacentes, usando um protocolo padrão. Esse controlador pode executar diversas aplicações de gerenciamento de rede, como o controle de acesso dinâmico, mobilidade transparente do usuário, engenharia de tráfego, balanceamento de carga do servidor, uso eficiente das redes em termos de energia e assim por diante. Acredito que a SDN é uma grande oportunidade para acertar o gerenciamento de rede, repensando a relação entre os dispositivos de rede e o software que os controla.
>
> Jennifer Rexford
> Entrevista no livro do Kurose, página 578

## Software-Defined Networking: A Comprehensive Survey (IEEE 2015)

Apesar de serem amplamente utilizadas, redes de IP tradicionais são muito complicadas e muito difíceis de serem gerenciadas. A dificuldade está em configurar a rede de acordo com as políticas pré-definidas e reconfigurá-las para corrigir falhas, excesso de carga e mudanças. Ainda, o pior é que essas redes são integradas verticalmente: os planos de controle e de dados são juntamente acoplados. As **Redes Definidas por Software (SDN)** formam um paradigma emergente que promete corrigir estas questões, desfazendo esta integração vertical, separando a lógica do tráfego de controle dos roteadores e *switches*, promovendo, logicamente, centralização da rede de controle, e tornando possível programar a rede. 

> O **comutador** (em inglês, ***switch***) é um dispositivo utilizado em [redes de computadores](https://pt.wikipedia.org/wiki/Rede_de_computadores) para reencaminhar [pacotes](https://pt.wikipedia.org/wiki/Pacotes) (*[frames](https://pt.wikipedia.org/wiki/Frames)*) entre os diversos nós. Possuem [portas](https://pt.wikipedia.org/wiki/Porta_(redes_de_computadores)), assim como os [concentradores](https://pt.wikipedia.org/wiki/Concentrador) (*hubs*), sendo que a principal diferença é o comutador segmentar a rede  internamente já que cada porta corresponde um domínio de colisão  diferente, eliminando assim a colisão entre pacotes de segmentos  diferentes. (Wikipédia)

A separação do plano de controle do plano de dados pode ser feita através de uma API bem definida entre os *switches* e o controlador SDN. O exemplo mais notável de uma API é o **OpenFlow**. Um *switch* OpenFlow tem um ou mais tabelas de pacotes lidando com regras (tabela de fluxo). Cada regra combina com um subconjunto do tráfego e execute certas ações (*dropping, forwarding, modifying etc.*) no tráfego. Dependendo das regras instaladas em uma aplicação por um controlador, um *switch* OpenFlow, instruído pelo controlador, pode se comportar como um roteador, *switch*, *firewall* ou executar outras regras (*e.g., load balance, traffic shaper* e, em geral, aquelas de uma *middlebox*). A forma de como o SDN é estruturado nos permite pensar *networking* utilizando dois conceitos principais, que são comuns em outras disciplinas de ciências de computação: **separação de questões/problemas** (alavancando o conceito de abstração) e **recursão**.

> A **middlebox** is a [computer networking](https://en.wikipedia.org/wiki/Computer_networking) device that transforms, inspects, filters, and manipulates traffic for purposes other than [packet forwarding](https://en.wikipedia.org/wiki/Packet_forwarding).[[1\]](https://en.wikipedia.org/wiki/Middlebox#cite_note-mboxtaxonomy-1) These extraneous functions have interfered with application performance and have been criticized for violating "important architectural  principles" such as the [end-to-end principle](https://en.wikipedia.org/wiki/End-to-end_principle). Examples of middleboxes include [firewalls](https://en.wikipedia.org/wiki/Firewall_(networking)), [network address translators](https://en.wikipedia.org/wiki/Network_address_translation) (NATs), [load balancers](https://en.wikipedia.org/wiki/Load_balancer), and [deep packet inspection](https://en.wikipedia.org/wiki/Deep_packet_inspection) (DPI) boxes. [(Wikipédia)](https://en.wikipedia.org/wiki/Middlebox)

Apesar de ter começado como objeto de pesquisa acadêmica, a maioria dos vendedores de *switches* comercial já incluem suporte para a API OpenFlow. Empresas como Google, Facebook, Yahoo, Microsoft, Verizon e Deutsche Telekom financiaram a ONF (*Open Networking Foundation*) com o objetivo principal de promover e adotar o SDN através de padrões de desenvolvimento abertos. A Google mesma, empregou o SDN para interconectar seus centros de dados ao redor do mundo.

### Status Quo das Redes

As redes de computadores podem ser basicamente dividas em três planos de funcionalidades: plano de dados, de controle e de gerenciamento, como mostrado abaixo.

<img src="Notas.assets/image-20210110232005204.png" alt="image-20210110232005204" style="zoom:80%;" />

* Plano de dados: dispositivos de rede, responsáveis por encaminhar os dados.
* Plano de controle: protocolos usados para popular as tabelas em encaminhamento dos elementos do plano de dados.
* Plano de gerenciamento: inclui os serviços de software, como as ferramentas base do protocolo simples de gerenciamento de rede (SNMP - *simple network management protocol*), usado para monitorar e configurar remotamente a funcionalidade de controle. Neste plano também é definida a política de rede, a qual o plano de controle faz cumprir, e o plano de dados executa essa política a medida que encaminha os dados apropriadamente.

Em redes de IP tradicionais, os planos de controle e de dados estão acoplados um ao outro, embarcados nos mesmos dispositivos de rede e portanto, toda a estrutura é altamente descentralizada. Este método tem sido bastante eficaz em termos de performance nos últimos anos, com um rápido aumento da taxa de transmissão de linha e densidade de portas. Entretanto, a desvantagem é uma arquitetura muito complicada e relativamente estática. A inovação neste cenário é portanto limitada.

 ### O quê são as Redes Definidas por Software?

As SDN são originalmente referentes à arquitetura de redes em que o estado de encaminhamento no plano de dados é gerenciado por um plano remotamente controlado separado do anterior. Definimos esta arquitetura utilizando quatro pilares:

1. Os planos de controle e de dados são separados. A funcionalidade de controle é removida dos dispositivos de rede que se tornarão simples elementos de encaminhamento de pacotes.
2. As decisões de encaminhamento são baseadas no fluxo, e não no destino. Um fluxo é normalmente definido por um conjunto de valores de campos de pacotes atuando como um critério de correspondência (filtro) e um conjunto de ações (instruções). No contexto de SDN/OpenFlow, um fluxo é uma sequência de pacotes entre a fonte e o destino. Todos os pacotes de um fluxo recebem políticas de serviços idênticas nos dispositivos de encaminhamento. Esta abstração nos permite unificar diferentes tipos de dispositivos, como roteadores, *switches*, *firewalls* e *middleboxes*.
3. A lógica de controle é movida para uma entidade externa, o chamado controlador SDN, ou NOS, uma plataforma de software que é executada em *commodity servers* e fornecem  os recursos essenciais e abstrações para facilitar a programação dos dispositivos de encaminhamento baseado em uma visão de rede abstrata logicamente centralizada. Seu propósito é parecido com o de um Sistema Operacional.
4. A rede pode ser programada através de aplicações de software rodando no topo do NOS que interage com os dispositivos de base do plano de dados. Isso é uma característica fundamental do SDN.

Uma SDN pode ser também definida através de três abstrações fundamentais: encaminhamento (*forwarding*), distribuição e especificação. O OpenFlow, parte da abstração de encaminhamento, pode ser visto como um driver de dispositivo em um SO.

A figura abaixo ilustra a arquitetura SDN, conceitos e blocos de construção da rede.

<img src="Notas.assets/image-20210111013602937.png" alt="image-20210111013602937" style="zoom:80%;" />

<img src="Notas.assets/image-20210111013827762.png" alt="image-20210111013827762" style="zoom:80%;" />

#### Terminologia

1. *Forwarding Devices (FD)*: Dispositivos de plano de dados, baseados em hardware ou software, que executam um conjunto de operações elementares. Possuem regras de fluxo que agem nos pacotes que estão chegando (*e.g.* reencaminhando para portas especificas, *dropping*, repassar para o controlador, reescrever algum cabeçalho etc.). Essas instruções são definidas por interfaces ***southbound***, como o **OpenFlow**, e são instaladas nos FDs pelos controladores SDN, implementando os protocolos *southbound*.
2. *Data Plate (DP):* FDs são interconectados através de canais de rádio sem fio ou por cabos. A infraestrutura da rede abrange esses FDs interconectados, que representam o plano de dados DP.
3. *Southbound Interface (SI):* API que define o conjunto de instruções dos FDs. Ainda, a SI também define o protocolo de comunicação entre FDs e elementos do plano de controle. Esse protocolo formaliza a forma como os elementos do plano de controle e do plano de dados interagem entre si.
4. *Control Plane (CP):* FDs são programados pelos elementos do CP através da incorporação bem definida do SI. O plano de controle CP pode então ser visto como o "cérebro da rede". Toda a lógica de controle está nas aplicações e nos controladores, que formam juntos o plano de controle.
5. *Northbound Interface (NI):* Os Sistemas Operacionais de Rede (NOS — Networking Operating Systems) podem oferecer uma API para os desenvolvedor de aplicações. Essa API representa a interface norte, como a interface comum para desenvolver aplicações. Tipicamente, a NI abstrai o conjunto de instruções de baixo nível usado pela SI para programar os FDs.
6. *Management Plane (MP):* O plano de gerenciamento é um conjunto de aplicações que influencia as funções oferecidas pelo NY para implementar o controle de rede e operações lógicas. Isso inclui aplicações como roteamento, *firewalls*, balanceadores de carga, monitoramento etc. Essencialmente, uma aplicação de gerenciamento define as políticas, que são finalmente traduzidas para instruções específicas do *southbound*, que programam o comportamento dos FDs.

Abaixo temos um modelo de como essa terminologia é integrada:
<img src="Notas.assets/image-20210111214731317.png" alt="image-20210111214731317" style="zoom:80%;" />

O plano de controle CP pode também ser visto como um ***Broker SDN***, uma abordagem que retém planos de controle distribuídos mas oferece novas APIs que permitam as aplicações interagirem a rede (bidireccionalmente). **Um controlador SDN frequentemente age como um *broker* entre as aplicações e os elementos de rede.** Esse método apresenta os dados do plano de controle para a aplicação e permite um certo grau de programabilidade de rede, em termos de "*plug-ins*" entre a função orquestradora do *broker* e os protocolos de rede. Esse modelo de API corresponde a um modelo híbrido de SDN, pois utiliza um *broker* para manipular e interagir diretamente com os planos de controle dos dispositivos como roteadores e *switches*. 

