# ANÁLISE E IMPLEMENTAÇÃO DE REDES DEFINIDAS POR SOFTWARE UTILIZANDO O OPENFLOW PARA OTIMIZAR O FLUXO DE MENSAGENS DE IIOT DESTINADAS A UM BROKER MQTT

[TOC]

******

## Introdução

Redes Definidas por Software , em inglês Software Defined Network (SDN), são estruturas de redes que utilizam controladores baseados em softwares ou Interfaces de Programação de Aplicativos, do inglês Aplication Programming Interface (API), que tem como objetivo direcionar o tráfego na rede e se comunicar com os hardwares ligados a ela. Essa abordagem permite criar e controlar uma rede virtual ou controlar uma rede tradicional de hardware por meio de software. Tal tipo de rede definida por software promove uma nova maneira de controlar as rotas de pacotes da rede, a fim de melhorar o desempenho da rede como um todo. É principalmente usada em aplicações industriais de maior porte, por necessitar de entrega de muitos pacotes com o mínimo de atraso ou erros possível.

Com o aumento da digitalização, ambientes industriais possuem alta demanda na transmissão de pacotes de redes, causando sobrecarga no fluxo geral de pacotes. O emprego de IIoT coloca em questão a confiabilidade na transmissão de mensagens entre dispositivos e máquinas industriais que podem se comportar como um sistema crítico. Neste sentido, é interessante analisar o emprego de um fluxo de controle de pacotes baseado em prioridades como forma de evitar o aumento da latência e perda de pacotes entre clientes MQTT.

Utilizando o simulador de redes Mininet e sua interface em Python, o objetivo deste trabalho é implementar uma arquitetura de rede SDN para prover priorização do fluxo de pacotes de rede destinados à aplicações IIoT no broker MQTT sobre outras fontes não-críticas. Desta forma, buscar reduzir a latência e perda de pacotes entre os clientes MQTT devido à sobrecargas na estrutura da camada de rede.

### Mininet

A mininet é um emulador de redes virtuais real rodando com kernel, switch e código de aplicação reais em uma única máquina virtual. Com essa ferramenta podemos gerenciar *switches*, controladores e *hosts* com a topologia desejada. 

![img](Manual.assets/image-20210112205113878.png)

A mininet nos oferece uma maneira de interagir com a rede mais acessível através de linhas de comando, além de poder customizar e aplicar em uma rede real posteriormente. O emulador também é uma ótima maneira de desenvolvimento utilizando o protocolo OpenFlow ou outros sistemas de redes definidas por software.

![img](Manual.assets/frontpage_diagram.png)

Uma máquina virtual é capaz de virtualizar diversos tipos de sistemas operacionais em um único computador. O VirtualBox da Oracle é a máquina virtual mais utilizada e com ela podemos instalar uma imagem da mininet rodando no sistema operacional Ubuntu 14.04 com todos os recursos e pacotes necessários para o desenvolvimento de nossa rede virtual sem mais complicações. 

### Sobre o Grupo

O grupo é formado pelos acadêmicos Lucas de Camargo, Leonardo Afonso e Stéfano Oliveira, da disciplina de Redes de Computadores da Universidade do Estado de Santa Catarina (UDESC) . 

# Manual de Instalação

Nesse manual elencamos os pacotes necessários para a realização do experimento. Tenha em mente que é necessário ao menos os requisitos básicos para a instalação dos demais pacotes. Utilizaremos a **Mininet** com a máquina virtual (**VirtualBox** da Oracle) e a ferramenta **Mosquitto** para utilização protocolo MQTT. Para a aplicação utilizaremos em Python 2.7 com a ferramenta **Paho MQTT**. Por fim, para a análise utilizaremos alguns pacotes básicos do Python junto com **Matplotlib**. Nos tópicos a seguir, mostraremos o passo-a-passo de como preparar esse ambiente todo.

## Requisitos básicos

Abaixo está listada a configuração utilizada no desenvolvimento do projeto, dada como requisito básico.

* [Linux Ubuntu 20.04 LTS](https://ubuntu.com/download/desktop)
* [Python 3.8](https://www.python.org/downloads/release/python-388/)

Outros sistemas operacionais com combinações de pacotes com versões diferentes não foram testados, portanto poderá haver algumas divergências.

## Mininet

Considerando o sistema operacional Linux Ubuntu 20.04, existem duas formas de se utilizar o Mininet:

1. Através de uma **máquina virtual** *e.g.* Oracle VM VirtualBox, recomendado pelos desenvolvedores, porém exige bastante espaço (~3 GB), pois utiliza uma versão do SO Ubuntu 14.04.4 LTS.
2. Instalando o Mininet do **repositório**.

Vamos fazer a instalação pelo VirtualBox.

### Download

1. Faça o download do VirtualBox para SO (Sistema Operacional) Linux. Baixe a imagem do software de acordo com a sua distrubição, diretamente do site oficial https://www.virtualbox.org/wiki/Linux_Downloads . Instale o pacote .deb baixado.

2. Faça o download da imagem do Mininet-VM, da página oficial https://github.com/mininet/mininet/releases . Recomenda-se a versão 2.2.2:

* [Virtual Machine Image (OVF format, 64-bit, Mininet 2.2.2)](https://github.com/mininet/mininet/releases/download/2.2.2/mininet-2.2.2-170321-ubuntu-14.04.4-server-amd64.zip) (Recomendado para a maioria dos hardwares modernos e sistemas operacionais)
* [Virtual Machine Image (OVF format, 32-bit, Mininet 2.2.2)](https://github.com/mininet/mininet/releases/download/2.2.2/mininet-2.2.2-170321-ubuntu-14.04.4-server-i386.zip) (Recomendado para usuários Windows 32 bits utilizando VirtualBox ou Hyper-V)

Extraia os arquivos `.ovf` e `.vmdk` em uma mesma pasta.

### Preparando a Máquina Virtual

Abra o software do VirtualBox, selecione `File > Import Appliance` e selecione o arquivo `.ovf` que você extraiu. Pressione o botão `Import` e mantenha as configurações padrões. Isso irá demorar alguns segundos, a imagem terá um tamanho de 3 GB.

Em seguida precisamos configurar a máquina virtual para termos acesso  externamente via SSH. Selecione `File > Host Network Manager`, clique em `Create`. Feche a janela clicando em `Close` para evitar o congelamento do VirtualBox.

Selecione a imagem Mininet-VM no menu lateral (**não execute!**), vá em `Settings > Network > Adapter 2` e marque a caixa `Enable Network Adapter`. Em `Attached to`, selecione `Host-only Adapter` e, em `Name`, selecione o adaptador criado no passo anterior, provavelmente será `vboxnet0`. Clique em`OK` e estamos prontos.

### Inicializando o Mininet-VM

Inicialize o Mininet-VM pelo VirtualBox e espere o boot da máquina. Quando pedir para inserir o usuário e senha, utilize:

* Usuário: `mininet`
* Senha: `mininet`

E então a máquina estará ligada. Entretanto, note que a janela do VirtualBox não é nada prático de ser utilizada. Como alternativa, utilizaremos uma janela SSH.

Na janela do Mininet-VM, digite manualmente no terminal `$ ifconfig eth0 | grep "inet addr:" ` e tome nota do endereço de IP de `inet addr`. Pode ser por exemplo, `192.168.56.102` ou `10.0.0.11`.

Em seguida, execute uma janela do terminal no seu SO e insira:

```bash
$ ssh -X mininet@<endereço-inet-addr>
```

O seu SO pode emitir uma mensagem dizendo que a autenticidade do host em questão não pode ser estabelecida e perguntará se você deseja prosseguir mesmo assim. Digite `yes`.

O SSH irá solicitar o *password* do usuário mininet. Este deverá ser `mininet`.

Em seguida, você estará conectado com a sua máquina virtual. Tente executar o comando `$ xterm`, que irá mostrar uma nova janela com outro terminal. Se der certo, estamos prontos! Caso você receba uma mensagem de erro `xterm: DISPLAY is not set error`, verifique a sua instalação do `X server`, geralmente já instalado nas versões do Linux.

### Compartilhando um diretório com o Mininet-VM

Isso torna o desenvolvimento muito prático, pois queremos preferencialmente utilizar nosso editor favorito no nosso SO e manter os arquivos conosco! Para isso, escolha um diretório qualquer no seu SO para ser compartilhado com o Mininet-VM e siga as instruções.

Abra o VirtualBox, com o Mininet-VM **desligado** e selecionado, vá em `Settings > Storage`. Em `Storage Devices`, selecione `Controller: SCSI`, clique no CDzinho com a label `Adds optical drive`. Na nova janela, selecione `VBoxGuestAdditions.iso` e clique em `Choose`. Caso não apareça o arquivo `.iso` para selecionar, clique em `Add`, navegue até o diretório `/usr/share/virtualbox` e selecione o arquivo `VBoxGuestAdditions.iso`.

Ainda na janela `Settings`, selecione `Shared Folders` na barra lateral e clique para inserir uma nova pasta no botão do lado direito. Selecione um diretório do seu computador, **marque** a opção `Auto Mount` e **desmarque** `Read Only`. Dê um nome para a sua pasta compartilhada, em `Mount point` insira `/media/shared` e clique em `OK`. Clique novamente em `OK` na janela de `Settings`.

Inicie o Mininet-VM. Digite:

```bash
$ sudo mount /dev/cdrom /media/cdrom
$ sudo /media/cdrom/./VBoxLinuxAdditions.run
$ sudo usermod -aG vboxsf $USER
$ sudo shutdown -r now
```

Espere a máquina reiniciar, faça o login novamente e crie um *link* simbólico da pasta compartilhada no diretório `/home` para facilitar o acesso:

``` bash
$ ln -s /media/shared /home/mininet/shared
```

E agora temos um diretório compartilhado que será montado automaticamente!

## MQTT

O MQTT é um protocolo de mensagens padrão para IoT (do inglês Internet of Things). Ele foi desenvolvido para ser um comunicador extremamente leve para publicar mensagens e recebê-las através de um mediante (broker). É ideal para conectar diversos tipos de dispositivos com o mínimo de código e o mínimo de largura de banda possível. Hoje na indústria é largamente utilizado em diversos setores como automotiva, manufatura, telecomunicações, dentre outras.

![image-20210301191725961](Manual.assets/image-20210301191725961.png)

Podemos ver na imagem acima um exemplo de uso simples do protocolo MQTT de um caso de sensor de temperatura com dois clientes agindo como ouvintes. Todas as informações são publicadas no broker pelo cliente ``publisher`` e são repassadas para os clientes ``subscribers`` que estiverem inscritos naquele tópico. Um tipo muito utilizado de broker é o ``Mosquitto``, o qual usaremos nesse trabalho.

### Mosquitto
O Eclipse Mosquitto é um *broker* de mensagens de código livre que implementa as versões 5.0, 3.1.1 e 3.1 do protocolo MQTT. Para a instalação, no sistema operacional, acesse o diretório compartilhado com a VM e baixe a o pacote de instalação do Mosquitto.

```bash
$ wget http://archive.ubuntu.com/ubuntu/pool/universe/m/mosquitto/mosquitto_0.15-2+deb7u3ubuntu0.1_amd64.deb
```
Na Mininet-VM, acesse o diretório compartilhado e faça a instalação do pacote.

```bash
$ sudo dpkg -i mosquitto_0.15-2+deb7u3ubuntu0.1_amd64.deb
```

O mosquitto broker será instalado em sua VM. Ao término da instalação, pode-se checar se o broker está ouvindo na porta padrão 1883 por meio do seguinte comando:

```bash
$ sudo netstat -tunlp | grep mosquitto
```

## Aplicação em Python

Nossa aplicação em Python tem como objetivo testar a rede proposta simulando os *hosts* como clientes do broker Mosquitto, enviando mensagens uns aos outros.  Podemos testar nossa rede enviando pacotes pelo protocolo MQTT e através de algumas métricas, conseguiremos dizer qual foi o desempenho de nossa rede com esses clientes.  

Para o desenvolvimento do programa que enviará as mensagens, utilizaremos a biblioteca **Paho MQTT** que é uma biblioteca em Python para essa finalidade de IoT. Além disso, para a análise dos resultados obtidos, utilizaremos algumas métricas de desempenho que serão representadas por gráficos da biblioteca **Matplotlib**. Ambas instalações estão demonstradas nos próximos tópicos

### Paho

Paho é uma ferramenta desenvolvida pela fundação Eclipse que tem como objetivo fornecer classes de clientes compatíveis com o protocolo MQTT v3.1 e v3.1.1 no Python 2.7 ou 3.x. Também propicia o desenvolvimento rápido e simples dos envios de pacotes entre clientes publishers e subscribers.

No **seu SO**, no diretório compartilhado insira o seguinte comando em um terminal:

```bash
$ wget https://github.com/eclipse/paho.mqtt.python/archive/master.zip
$ unzip ./master.zip
```

Na **máquina virtual** (pode ser através do seu terminal com SSH conectado a máquina virtual):

```bash
$ cd ./paho.mqtt.python-master
$ python setup.py install
```

### Análise gráfica

Para a visualização dos testes realizados, precisaremos da biblioteca matplotlib instalada em **seu SO**. Essa biblioteca pode ser facilmente instalada digitando o seguinte comando no seu terminal:

``` bash
$ python3 -m pip install -U matplotlib
```

## Q&A

**1. A aplicação funciona em sistema operacional Windows ou macOS?**  
    Considerando a execução do Mininet em uma máquina virtual utilizando o software VirtualBox, disponível também para Windows e macOS, a aplicação deve também funcionar. Entretanto, alguns passos deste manual podem diferir.

**2. É necessário criar um diretório compartilhado?**  
    Não abordamos aqui a configuração de acesso à Internet na máquina virtual, pois pode diferir de usuário para usuário. Portanto criamos um diretório compartilhado para compartilhar os arquivos de download com o Mininet. Outra alternativa seria a transferência do arquivo via SSH.

# Referências

KREUTZ, D.; RAMOS, F. M. V.; VERÍSSIMO, P. E.; ROTHENBERG, C. E.; AZODOLMOLKY, S.; UHHLIG, S. Software-Defined Networking: A Comprehensive Survey, in Proceedings of the IEEE, vol. 103, no. 1, pp. 14-76, Jan. 2015, [doi: 10.1109/JPROC.2014.2371999](https://ieeexplore.ieee.org/document/6994333).

Mininet: An Instant Virtual Network on your Laptop (or other PC) - Slides da Unicamp  
https://www.ic.unicamp.br/~nfonseca/MO648/doc/Introduction%20to%20Mininet.pdf

Mininet.org - Walkthrough  
http://mininet.org/walkthrough/

OpenFlow Tutorial - GitHub Wiki  
https://github.com/mininet/openflow-tutorial/wiki

POX  
https://noxrepo.github.io/pox-doc/html/

Introduction to Mininet (David Mahler) - YouTube  
https://youtu.be/jmlgXaocwiE

Introduction to OpenFlow (David Mahler) - YouTube  
https://youtu.be/l25Ukkmk6Sk

Mosquitto  
https://mosquitto.org/

Matplotlib  
https://matplotlib.org/

Paho MQTT  
https://www.eclipse.org/paho/index.php?page=clients/python/index.php 
