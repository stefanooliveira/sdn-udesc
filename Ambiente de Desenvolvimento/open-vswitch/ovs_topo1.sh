#!/bin/bash 

# Minimum and maximum values for queues
RT_MAX=100000000
Q0_MIN=80000000
Q0_MAX=$RT_MAX
Q1_MIN=20000000
Q1_MAX=$RT_MAX

# Creating list of switches
list='s1-eth1 s1-eth2 s1-eth3 s1-eth4 s2-eth1 s2-eth2 s2-eth3'

# Iterate over list and creating queues
for i in $list; do
sudo ovs-vsctl -- set Port $i qos=@newqos -- \
--id=@newqos create Qos type=linux-htb other-config:max-rate=$RT_MAX queues=0=@q0,1=@q1 -- \
--id=@q0 create Queue other-config:min-rate=$Q0_MIN other-config:max-rate=$Q0_MAX -- \
--id=@q1 create Queue other-config:min-rate=$Q1_MIN other-config:max-rate=$Q1_MAX
done
