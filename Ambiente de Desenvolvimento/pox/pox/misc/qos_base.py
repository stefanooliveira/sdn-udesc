from pox.core import core
import pox.openflow.libopenflow_01 as of
import pox.lib.packet as pkt
import pox.lib.addresses as adr
from pox.lib.util import dpid_to_str

log = core.getLogger()

class QoSBaseController(object):
    def __init__(self, event):
        self.dpid = dpid_to_str(event.dpid)
        self.connection = event.connection
        log.debug("New connection of type " + str(type(self.connection)))
        log.debug("DpID: " + self.dpid)
        self.connection.addListeners(self)
        # Use this table to keep track of which ethernet address is on
        # which switch port (keys are MACs, values are ports).
        self.mac_to_port = {} # MAC address keys to port number values

    def resend_packet (self, packet_in, out_port):
        """
        Instructs the switch to resend a packet that it had sent to us.
        "packet_in" is the ofp_packet_in object the switch had sent to the
        controller due to a table-miss.
        """
        msg = of.ofp_packet_out()
        msg.data = packet_in
        # Add an action to send to the specified port
        action = of.ofp_action_output(port = out_port)
        msg.actions.append(action)
        # Send message to switch
        self.connection.send(msg)


    def switch(self, packet, packet_in):
        """
        Switch-like behavior from Tutorial
        """
        src_mac_addr = str(packet.src)
        dst_mac_addr = str(packet.dst)
        # Port on which frame was received
        input_port = packet_in.in_port # ofp_packet_in
        # Log
        log.debug("SW_CALL[{}]: {}-{}/{}".format(self.dpid,src_mac_addr,input_port,dst_mac_addr))
        log.debug("PCKT_TYPE[{}]: {}".format(self.dpid,packet.type))
        # Learn port for source MAC
        self.mac_to_port[src_mac_addr] = input_port
        # If the port associated with the destination MAC of the packet is known:
        if dst_mac_addr in self.mac_to_port:
            out_port = self.mac_to_port[dst_mac_addr]
            log.debug("FLOW_INST[{}]: {}-{}/{}-{}".format(self.dpid,src_mac_addr,input_port,dst_mac_addr,out_port))
            
            msg = of.ofp_flow_mod()
            msg.command = of.OFPFC_MODIFY
            msg.priority = 10
            msg.match = of.ofp_match.from_packet(packet)
            msg.idle_timeout = 10
            msg.hard_timeout = 60
            msg.buffer_id = packet_in.buffer_id
            msg.actions.append(of.ofp_action_output(port=out_port))
            self.connection.send(msg)

        else:
            # Flood the packet out everything but the input port
            self.resend_packet(packet_in, of.OFPP_ALL)

    
    def _handle_PacketIn(self, event):
        """
        Handles packet in messages from the switch.
        """
        packet = event.parsed
        if not packet.parsed:
            log.warning("Ignoring incomplete packet")
            return
        packet_in = event.ofp # The actual ofp_packet_in message.
        self.switch(packet, packet_in)

def launch(port=6633):
    """
    Starts the component
    """
    
    from pox.openflow.of_01 import launch
    launch(port=port)

    def start_switch (event):
        log.debug("Controlling %s" % (event.connection,))
        QoSBaseController(event)

    core.openflow.addListenerByName("ConnectionUp", start_switch)
