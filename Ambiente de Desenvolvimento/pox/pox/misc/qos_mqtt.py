from pox.core import core
import pox.openflow.libopenflow_01 as of
import pox.lib.packet as pkt
import pox.lib.addresses as adr
from pox.lib.util import dpid_to_str
from pox.misc.qos_base import QoSBaseController

log = core.getLogger()

class QoSMQTTController(QoSBaseController):
    def qos_mqtt(self, packet, packet_in):
        src_mac_addr = str(packet.src)
        dst_mac_addr = str(packet.dst)
        input_port = packet_in.in_port # ofp_packet_in

        if dst_mac_addr in self.mac_to_port:
            out_port = self.mac_to_port[dst_mac_addr]

            mqtt_msg = of.ofp_flow_mod()
            mqtt_msg.command = of.OFPFC_MODIFY
            mqtt_msg.priority = 1000
            mqtt_msg.match = of.ofp_match()
            mqtt_msg.match.dl_src = packet.src
            mqtt_msg.match.dl_dst = packet.dst
            mqtt_msg.match.dl_type = pkt.ethernet.IP_TYPE
            mqtt_msg.match.nw_proto = pkt.ipv4.TCP_PROTOCOL
            mqtt_msg.match.tp_src = None
            mqtt_msg.match.tp_dst = 1883 # to broker
            mqtt_msg.idle_timeout = 10
            mqtt_msg.hard_timeout = 60
            mqtt_msg.actions.append(of.ofp_action_enqueue(port=out_port, queue_id=1))
            self.connection.send(mqtt_msg)

            mqtt_msg.match = mqtt_msg.match.flip()
            del mqtt_msg.actions
            mqtt_msg.actions = []
            mqtt_msg.actions.append(of.ofp_action_enqueue(port=input_port, queue_id=1))
            self.connection.send(mqtt_msg)


    def switch(self, packet, packet_in):
        """
        Switch-like behavior from Tutorial
        """
        QoSBaseController.switch(self,packet,packet_in)
        self.qos_mqtt(packet, packet_in)

def launch(port=6633):
    """
    Starts the component
    """

    from pox.openflow.of_01 import launch
    launch(port=port)

    def start_switch (event):
        log.debug("Controlling %s" % (event.connection,))
        QoSMQTTController(event)

    core.openflow.addListenerByName("ConnectionUp", start_switch)
