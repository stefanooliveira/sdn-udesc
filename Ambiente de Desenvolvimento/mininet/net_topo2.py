from mininet.net import Mininet
from mininet.topo import Topo
from mininet.node import Controller, OVSSwitch, RemoteController
from mininet.cli import CLI
from mininet.log import setLogLevel, info

def main():
    # Delcarando a Mininet
    net = Mininet(controller=None, waitConnected=True)

    # Declarando controlador(es) remoto(s) para o nosso controlador POX
    info( "*** Creating (reference) controllers\n" )
    c0 = RemoteController('c0', ip="127.0.0.1", port=6633)
    c1 = RemoteController('c1', ip="127.0.0.1", port=6653)
    info(repr(c0),"\n")
    info(repr(c1),"\n")
    
    # Adiciona Hosts e Switches
    info( "*** Creating hosts\n" )
    net.addHost( 'h1' )
    net.addHost( 'h2' )
    net.addHost( 'h3' )
    net.addHost( 'h4' )
    net.addHost( 'h5' )
    net.addSwitch( 's1' )
    net.addSwitch( 's2' )
    net.addSwitch( 's3' )

    for i in net.keys():
        info(repr(net[i]),"\n")

    # Estabelece Links
    info( "\n*** Creating links\n" )
    net.addLink( net['s2'], net['s3'] )
    net.addLink( net['s1'], net['s3'] )
    net.addLink( net['h1'], net['s1'] )
    net.addLink( net['h2'], net['s2'] )
    net.addLink( net['h3'], net['s2'] )
    net.addLink( net['h4'], net['s3'] )
    net.addLink( net['h5'], net['s3'] )

    for i in net.links:
        info(str(i),"\n")

    # Inicializa a rede
    info( "\n*** Starting network\n" )
    net.build()

    # Inicializa os switches com os controladores
    net['s1'].start([c0])
    net['s2'].start([c0])
    net['s3'].start([c1])

    # Inicializa o CLI
    info( "\n*** Running CLI\n" )
    CLI(net)
    
    # Finaliza
    info( "\n*** Stopping network\n" )
    net.stop()

if __name__=="__main__":
    setLogLevel( 'info' )  # for CLI output
    main()
