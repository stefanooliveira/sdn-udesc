import argparse
import time
import paho.mqtt.client as mqtt

__protocol_versions__ = {"v31": mqtt.MQTTv31, "v311": mqtt.MQTTv311, "v5": mqtt.MQTTv5}
__start__ = time.time()

def get_time():
    return time.time() - __start__

class Subscriber(object):
    class Publisher:
        def __init__(self, name, seq_number, output='./'):
            self.name = name
            self.seq_delta = seq_number # Sequence number
            self.file = None
            self.output = output
        
        def __del__(self):
            if self.file is not None:
                self.file.close()
        
        def consume(self, seq_number, timestamp):
            delta = (time.time() - timestamp) * 1000 # ms
            self._entry_consume("{},{},{}\n".format(get_time(), delta, seq_number - self.seq_delta))

        def _entry_consume(self, entry):
            if self.file is None:
                self.file = open(self.output + "{}.csv".format(self.name), 'w+')
            self.file.write(entry)
            

    def __init__(self, **kwargs):
        self.host = kwargs.get('host', "localhost")
        self.port = kwargs.get('port', 1883)
        self.time = kwargs.get('time', 0)
        self.topic = kwargs.get('topic',"")
        self.qos = kwargs.get('qos', 0)
        self.name = kwargs.get('name')
        self.version = kwargs.get('version')
        self.output = kwargs.get('output', "./")
        self.client = mqtt.Client(client_id=self.name, protocol=self.version)
        self.client.on_connect = self._on_connect
        self.client.on_message = self._on_message
        self.name = self.client._client_id.decode('utf-8')
        self.output +=  self.name + "_"
        self.time_ctl = 0
        self.publishers = dict()

    def _on_connect(self, client, userdata, flags, rc):
        if rc==0:
            print("Connected to {}:{}".format(self.host,self.port))
        else:
            raise ConnectionError("Could not connect to {}:{}".format(self.host,self.port))

    def _on_message(self, client, userdata, msg):
        msg = msg.payload.decode()
        msg = msg.split(',')
        # print(msg)
        if msg[0] not in self.publishers:
            self.publishers[msg[0]] = Subscriber.Publisher(msg[0],int(msg[1]),self.output)
        self.publishers[msg[0]].consume(int(msg[1]),float(msg[2]))

    def loop(self):
        if self.time:
            if self.time_ctl == 0:
                self.time_ctl = time.time()
            if (time.time() - self.time_ctl) >= self.time:
                return False
        self.client.loop()
        return True

    def connect(self):
        self.client.connect(self.host, self.port)
        self.client.subscribe(self.topic,self.qos)

    def disconnect(self):
        self.client.disconnect()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-ho", "--host", help="MQTT broker address e.g. 192.168.0.1", type=str, default="localhost")
    parser.add_argument("-p", "--port", help="MQTT broker port (def. 1883)", type=int, default=1883)
    parser.add_argument("-to", "--topic", help="MQTT root topic (def. benchmark", type=str, default="benchmark")
    parser.add_argument("-n", "--name", help="Client name", type=str)
    parser.add_argument("-q", "--qos", help="MQTT Quality of Service (def. 0)", choices=[0,1,2], type=int, default=0)
    parser.add_argument("-t", "--time", help="Run for t seconds", type=int, default=0)
    parser.add_argument("-v", "--version", help="MQTT protocol version (def. v31", choices=['v31', 'v311', 'v5'], type=str, default='v31')
    parser.add_argument("-o", "--output", help="Path to output file e.g. ./folder/ or ./folder/file_prefix_", type=str, default="./")
    args = parser.parse_args()
    args.version = __protocol_versions__[args.version]

    subs = Subscriber(**args.__dict__)
    subs.connect()
    while subs.loop():
        pass
    subs.disconnect()
