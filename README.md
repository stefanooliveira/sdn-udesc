# SDN UDESC

## Redes Definidas por Software (SDN) com OpenFlow
******
### Tópicos mínimos a serem abordados:
1. Definição, histórico e componentes;
2. Definição funcionamento, arquitetura e serviços;
3. Tipos de Planos de Dados e Planos de Controle;
4. Mininet;
5. Análise das implementações disponíveis; e
6. Casos comentados (um feito pela equipe e outro referenciado).
******
### Equipe:
1. Lucas Camargo
2. Leonardo Afonso
3. Stéfano Oliveira
******
